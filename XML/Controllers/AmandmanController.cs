﻿using DAL.Repository;
using DAL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace XML.Controllers
{
    public class AmandmanController : ApiController
    {



        UnitOfWork unit = new UnitOfWork();
        GenericRepository<Amandman> amandmanRepository = null;


        public AmandmanController()
        {

            this.amandmanRepository = unit.AmandmanRepository;
         }



        // GET: api/Amandman
        public IEnumerable<Amandman> Get()
        {
            try
            {
                return amandmanRepository.Get();
            }
            catch (Exception e)
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        // GET: api/Amandman/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Amandman
        public void Post(NoviAmandman noviAmandman)
        {

            NoviAmandman am = noviAmandman;
            try
            {       
            Amandman aman = am.noviAmandman;
            amandmanRepository.Insert(aman);
            }catch(Exception e)
            {
                throw new HttpResponseException(
                   Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }



        }

        // PUT: api/Amandman/5
        public void Put(int id, [FromBody]string value)
        {





        }

        // DELETE: api/Amandman/5
        public void Delete(string id)
        {
           

            try
            {
                amandmanRepository.Delete(id);
        
            }
            catch (Exception e)
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }
    }
}
