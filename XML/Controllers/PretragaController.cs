﻿using DAL.Repository;
using DAL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace XML.Controllers
{
    public class PretragaController : ApiController
    {

        UnitOfWork unit = new UnitOfWork();
        GenericRepository<Akt> aktRepository = null;


        public PretragaController()
        {

            aktRepository = unit.AktRepository;

        }

        // GET: api/Pretraga
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Pretraga/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Pretraga
        public IEnumerable<Akt> Post(SearchQuery query)
        {
            if (query == null)
                return null;

            IEnumerable<Akt> akt = new List<Akt>();
            try
            {

                if (query.TipPretrage.Equals("metapodaci"))
                {

                    string getMeta = "xquery version \"1.0-ml\"; declare namespace ak = \"aktovi\"; " +
                           " for $b in fn:collection(\"/aktovi\")/ak:Akt " +
                           "where fn:contains($b/ak:"+ query.Metapodatak+", \""+ query.Tekst+"\") = fn:true()  " +
                                   "return  $b ";

                    akt = aktRepository.DoQueryWithRetyrn(getMeta);


                }
                else if (query.TipPretrage.Equals("tekst"))
                {
                    string getText = "xquery version \"1.0-ml\"; declare namespace ak = \"aktovi\"; " +
                           " for $b in fn:collection(\"/aktovi\") " +
                            "return  if (fn:contains(fn:string-join($b//ak:Tekst),\"" + query.Tekst + "\") = fn:true()) then $b  " +
                                  "else() ";
                    akt = aktRepository.DoQueryWithRetyrn(getText);

                }
            }
            catch (Exception e)
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }

            return akt;

        }

        // PUT: api/Pretraga/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Pretraga/5
        public void Delete(int id)
        {
        }
    }
}
