﻿using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using AttributeRouting.Web.Http;
using System.Xml;

namespace XML.Controllers
{
    public class AktController : ApiController
    {
        
        UnitOfWork unit = new UnitOfWork();
        GenericRepository<Akt> aktRepository = null;


        public AktController()
        {
            aktRepository = unit.AktRepository;
            

        }


        // GET: api/Akt
        public IEnumerable<Akt> Get()
        {
            try
            {
               return aktRepository.Get();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        // GET: api/Akt/5
        public Akt Get(string id)
        {
            try
            {
                Akt akt = aktRepository.GetById(id);
                if (akt == null)
                    throw new HttpResponseException(
                   Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Ne postoji akt sa trazenim id-em"));
                else
                    return akt;

            }
            catch(Exception e)
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        // POST: api/Akt
        public void Post(Akt value)
        {
            System.Diagnostics.Debug.Write(value.Naziv);

            try
            {
                aktRepository.Insert(value);
            }catch(Exception e)
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }

        }

        

        // PUT: api/Akt/5
        public void Put(string id, [FromBody]AktStanje val)
        {

            string value = val.ToString();

            if (id == null || value == "" || value == null)
                return ;
            string getMeta = "xquery version \"1.0-ml\"; declare namespace ak = \"aktovi\"; " +
                          " xdmp:node-replace(fn:doc(\"" + id + "\")/ak:Akt/ak:Stanje, <Stanje xmlns='aktovi'>" + value +
                          "</Stanje>);";

            try
            {
                aktRepository.DoQuery(getMeta);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }

        }

        // DELETE: api/Akt/5
        public void Delete(string id)
        {


            string deleteAmandmans = "xquery version \"1.0-ml\"; declare namespace ns0 = \"amandmani\"; " +
                   " for $b in fn:collection(\"/amandmani\")/ns0:Amandman " +
                   "where $b/ns0:Sadrzaj/@idAkta = \""+id+"\" " +
                           "return  xdmp:document-delete(base-uri($b)) ";


            try
            {
                aktRepository.DoQuery(deleteAmandmans);
                aktRepository.Delete(id);

            }
            catch (Exception e)
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }


  

    }
}
