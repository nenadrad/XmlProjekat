﻿using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace XML.Controllers
{
    public class HomeController : Controller
    {

        UnitOfWork  unit = new UnitOfWork();

        GenericRepository<Akt> repositoryAkt = null;

        public ActionResult Index()
        {


            ViewBag.Title = "Skupstina grada Novog Sada";

            /*try
            {
                repositoryAkt.Delete("pro");
            }catch(Exception e)
            {
                Debug.Write(e.Message);

            }*/

            return View();
        }

        public HomeController()
        {
            repositoryAkt = unit.AktRepository;
        }

        public ActionResult PredAktaTemplate()
        {
            return View("~/Views/Templates/PredAktaTemplate.cshtml");
        }

        public ActionResult PregledAkataTemplate()
        {
            return View("~/Views/Templates/PregledAkataTemplate.cshtml");
        }

        public ActionResult PretragaTemplate()
        {
            return View("~/Views/Templates/PretragaTemplate.cshtml");
        }

        public ActionResult SednicaTemplate()
        {
            return View("~/Views/Templates/SednicaTemplate.cshtml");
        }

        public ActionResult LoginForm() 
        {
            return View("~/Views/Templates/LoginForm.cshtml");
        }

        public ActionResult RegisterForm() 
        {
            return View("~/Views/Templates/RegisterForm.cshtml");
        }

    }

    
}
