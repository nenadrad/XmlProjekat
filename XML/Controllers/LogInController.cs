﻿using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DAL.Model;
using System.Xml;
using DAL.Utils;

namespace XML.Controllers
{
    public class LogInController : ApiController
    {

        UnitOfWork unit = new UnitOfWork();
        GenericRepository<Korisnici> korisnikRepository = null;
        XMLConverter<TKorisnik> konvert = new XMLConverter<TKorisnik>();
        public LogInController()
        {

            korisnikRepository = unit.KorisniciRepository;

        }

        // GET: api/LogIn
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/LogIn/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/LogIn
        public TKorisnik Post( KorisnikInput input)
        {
            TKorisnik korinik = null;
            if (input == null)
                return null;

            string logIn = "xquery version \"1.0-ml\"; declare namespace ko = \"korisnici\"; " +
                   " for $b in fn:doc(\"korisnicii\")//ko:Korisnik " +
                   "where $b/ko:KorisnickoIme = \"" + input.username + "\"  and $b/ko:Lozinka = \"" +input.password +"\""+
                           " return  $b ";

            
            try
            {
                string korisnikStr = korisnikRepository.DoQueryRetString(logIn);

                if (korisnikStr == null)
                    return null;
                XmlDocument doc = XMLValidation.ValidationBySchema(korisnikStr, SchemasID.KORISNICI_SCHEMA, SchemasID.KORISNICI_NAMESPACE);
                korinik = konvert.Deserialize(korisnikStr);

                return korinik;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }


        }

        // PUT: api/LogIn/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/LogIn/5
        public void Delete(int id)
        {
        }
    }
}
