﻿using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace XML.Controllers
{
    public class TransformController : ApiController
    {



        UnitOfWork unit = new UnitOfWork();
        GenericRepository<Akt> aktRepository = null;
        // GET: api/Transform
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        public TransformController()
        {
            aktRepository = unit.AktRepository;
        }
        
        // GET: api/Transform/5
        public string Get(string id)
        {
        
            try
            {
                return aktRepository.GetXslString(id);

            }
            catch (Exception e)
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }

        }

        // POST: api/Transform
        public void Post(string value)
        {
            try
            {
                string str =  aktRepository.GetXslString(value);

            }
            catch (Exception e)
            {
                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        // PUT: api/Transform/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Transform/5
        public void Delete(int id)
        {
        }
    }
}
