﻿var application = angular.module("app");

application.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
    .state('Home', {
        url: '/Home',
    })
    .state('login', {
        url: '/login',
        templateUrl: 'home/loginForm',
        controller: 'loginController',
    })
    .state('register', {
        url: '/Register',
        templateUrl: 'home/registerForm',
        controller: 'registerController',
    })
    .state('predlaganjeAkta', {
        url: '/predlaganjeAkta',
        templateUrl: 'home/predaktatemplate',
        controller: 'predAktaController'
    })
    .state('pregledAkata', {
        url: '/pregledAkata',
        templateUrl: 'home/pregledakatatemplate',
        controller: 'pregledAkataController'
    })
    .state('pretraga', {
        url: '/pretraga',
        templateUrl: 'home/pretragatemplate',
        controller: 'pretragaController',
        params: {
            tip: 'defaultvalue'
        }
    })
    .state('sednica', {
        url: '/sednica',
        templateUrl: 'home/sednicatemplate',
        controller: 'sednicaController'
    })
}]);