﻿var application = angular.module("app");

application.run(function ($rootScope, $http, $location, $localStorage, authenticationFactory, $state) {

    if ($localStorage.currentUser) {
        $http.defaults.headers.common.Authorization = $localStorage.currentUser.token;
    }

    // ukoliko pokušamo da odemo na stranicu za koju nemamo prava, redirektujemo se na login
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        var publicStates = ['login', 'register',/*'entry',*/''];
        var restrictedState = publicStates.indexOf(toState.name) === -1;
        if (restrictedState && !authenticationFactory.getCurrentUser()) {
            $state.go('login');
        }
    });

    $rootScope.logout = function () {
        authenticationFactory.logout();
    }

    $rootScope.getCurrentUserRole = function () {
        if (!authenticationFactory.getCurrentUser()) {
            return undefined;
        }
        else {
            return authenticationFactory.getCurrentUser().role;
        }
    }
    $rootScope.isLoggedIn = function () {
        if (authenticationFactory.getCurrentUser()) {
            return true;
        }
        else {
            return false;
        }
    }
    $rootScope.getCurrentState = function () {
        return $state.current.name;
    }
});