﻿pretragaController = function ($scope, $state, $stateParams, $sce, pretragaFactory, aktFactory) {
    console.log($stateParams["tip"]);

    $scope.tekst = false;
    $scope.metapodaci = false;
    $scope.showResults = false;

    $scope.results = [];

    var tip = $stateParams["tip"];
    if (tip == 'metapodaci') {
        $scope.metapodaci = true;
        $scope.tekst = false;
    }
    else if (tip == 'tekst') {
        $scope.tekst = true;
        $scope.metapodaci = false;
    }

    $scope.submit = function () {
        var data = {};

        if ($scope.metapodaci) {
            data = {
                TipPretrage: 'metapodaci',
                Metapodatak: $scope.selectMeta,
                Tekst: $scope.inputMeta
            }

            console.log(data);
        }
        else if ($scope.tekst) {
            data = {
                TipPretrage: 'tekst',
                Metapodatak: null,
                Tekst: $scope.inputTekst
            }

            console.log(data);
        }

        pretragaFactory.search(data)
            .then(function (response) {
                console.log(response.data);
                $scope.results = response.data;
                $scope.showResults = true;
            }, function (error) {
                toastr.error("Greska: " + error.data);
            });

    }

    $scope.prikaziAkat = function (id) {
        console.log(id);

        if ($scope.metapodaci)
            $scope.metapodaci = false;
        else if ($scope.tekst)
            $scope.tekst = false;
        
        $scope.showResults = false;
        $scope.showDocument = true;

        //$scope.htmlDokument = $sce.trustAsHtml("<h1>Sadrzaj akta</h1>");

        aktFactory.getHtmlById(id)
            .then(function (response) {
                console.log(response.data);
                $scope.htmlDokument = $sce.trustAsHtml(response.data);
                $scope.showDocument = true;
            }, function (error) {
                toastr.error("Greska: " + error.data);
            });
    }

    $scope.close = function () {
        console.log($stateParams["tip"]);

        $scope.showDocument = false;
        $scope.showResults = true;

        if ($stateParams["tip"] == 'metapodaci')
            $scope.metapodaci = true;
        else
            $scope.tekst = true;
       
    }
}

aktModule = angular.module("aktModule");
aktModule.controller('pretragaController', ['$scope', '$state', '$stateParams', '$sce', 'pretragaFactory', 'aktFactory', pretragaController]);