﻿mainController = function ($rootScope,$state, $scope) {
    if ($rootScope.isLoggedIn() == false) {
        $state.go('login');
    }

    $scope.changeState = function(value) {
        console.log(value);
        $state.go('pretraga', {tip : value});
    }
};

var app = angular.module("app");
app.controller('mainController', ['$rootScope', '$state', '$scope', mainController]);