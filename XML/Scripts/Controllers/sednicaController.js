﻿sednicaController = function ($scope, aktFactory, amandmanFactory) {
   
    $scope.akati = [];
    $scope.amandmani = [];

    getAll();

    function getAll() {
        aktFactory.getAll()
            .then(function (response) {
                $scope.akati = response.data;
                console.log(response.data)
            }, function (error) {
                toastr.error("Greska: " + error.data);
            });

        amandmanFactory.getAll()
            .then(function (response) {
                $scope.amandmani = response.data;
            }, function (error) {
                toastr.error("Greska: " + error.data);
            });
    }

    $scope.showListAkt = true;
    $scope.showListAm = false;
    $scope.odluka = false;
    $scope.glasanje = false;

    $scope.aktFilter = function (akat) {
        return akat.stanjeField == 1 || akat.stanjeField == 3;
    };

    $scope.prikaziAkat = function (akat) {
        $scope.trenutniAkat = akat;
        $scope.showListAkt = false;

        if (akat.stanjeField == 1)
            $scope.odluka = true;
        else if (akat.stanjeField == 3)
            zapocniGlasanje();
        
    }

    $scope.glasanjeUNacelu = function () {

        console.log($scope.trenutniAkat);
        $scope.trenutniAkat.stanjeField = 3;

        var data = $scope.trenutniAkat;

        aktFactory.update(data.idField, data.stanjeField)
            .then(function (response) {
                toastr.success("Glasanje u načelu.");
                zapocniGlasanje();
            }, function (error) {
                toastr.error("Greska: " + error.data);
            });

        zapocniGlasanje();
    }

    function zapocniGlasanje() {

        $scope.odluka = false;
        $scope.glasanje = true;

        $scope.amandmaniAkta = [];
        for (var i = 0; i < $scope.amandmani.length; i++) {
            if ($scope.amandmani[i].sadrzajField.idAktaField == $scope.trenutniAkat.idField) {
                $scope.amandmaniAkta.push($scope.amandmani[i]);
            }
        }
    }

    $scope.odbijPredlog = function () {
        var data = $scope.trenutniAkat;
        data.stanjeField = 2;;
        console.log(data);

        $scope.odluka = false;
        $scope.showListAkt = true;

        aktFactory.update(data.idField, data.stanjeField)
            .then(function (response) {
                toastr.success("Predlog odbijen.");
                $scope.odluka = false;
                $scope.showListAkt = true;
            }, function (error) {
                toastr.error("Greska: " + error.data);
            });
    }
}

var app = angular.module("app");
app.controller('sednicaController', ['$scope', 'aktFactory', 'amandmanFactory', sednicaController]);