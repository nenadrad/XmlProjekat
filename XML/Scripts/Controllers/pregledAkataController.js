﻿pregledAkataController = function ($scope, $sce, $state, aktFactory, amandmanFactory) {
    
    $scope.showList = true;
    $scope.showDocumentAkt = false;
    $scope.showDocumentAm = false;
    $scope.showForm = false;
    $scope.showListAm = false;
    
    getAll();

    function getAll() {
        aktFactory.getAll()
            .then(function(response) {
                $scope.akati = response.data;
            }, function(error) {
                toastr.error("Greska: "+error.data); 
            });

        amandmanFactory.getAll()
            .then(function (response) {
                $scope.amandmani = response.data;
            }, function (error) {
                toastr.error("Greska: " + error.data);
            });
    }
    
    
    $scope.prikaziAkat = function(id) {
        console.log(id);

        $scope.showList = false;
        $scope.showDocumentAkt = true;
        $scope.showForm = false;

        $scope.htmlDokument = $sce.trustAsHtml("<h1>Sadrzaj akta</h1>");
        
        aktFactory.getHtmlById(id)
            .then(function(response) {
                console.log(response.data);
                $scope.htmlDokument = $sce.trustAsHtml(response.data);
            }, function(error) {
                toastr.error("Greska: "+error.data); 
            });
    }

    $scope.predloziAmandman = function (data) {
        $scope.trenutniAkat = data;
        $scope.showList = false;
        $scope.showDocument = false;
        $scope.showListAm = false;
        $scope.showForm = true;
    }

    $scope.submit = function() {
        console.log($scope.dokument);

        data = {
            idAkta: $scope.trenutniAkat.idField,
            amandman: $scope.dokument
        };

        console.log(data);

        amandmanFactory.insert(data)
            .then(function (response) {
                getAll();
                $scope.dokument = "";
                toastr.success("Amandamn uspešno predložen");
            }, function (error) {
                toastr.error("Greska: " + error.data);
            });
    }

    $scope.obrisiAkat = function (id) {
        console.log(id);
        
        aktFactory.delete(id)
            .then(function (response) {
                for (var i = 0; i < $scope.akati.length; i++)
                    if ($scope.akati[i].idField == id)
                        $scope.akati.splice(i, 1);
                toastr.success("Uspesno povučen predlog.");
            }, function (error) {
                toastr.error("Greska: " + error.data);
            });
    }

    $scope.prikaziAmandmane = function (data) {
        $scope.trenutniAkat = data;

        $scope.amandmaniAkta = [];
        for (var i = 0; i < $scope.amandmani.length; i++) {
            if ($scope.amandmani[i].sadrzajField.idAktaField == $scope.trenutniAkat.idField) {
                $scope.amandmaniAkta.push($scope.amandmani[i]);
            }
        }

        $scope.showList = false;
        $scope.showListAm = true;
    }

    $scope.prikaziAmandman = function (id) {
        console.log(id);

        $scope.htmlDokument = $sce.trustAsHtml("<h1>Sadrzaj amandmana</h1>");

        $scope.showListAm = false;
        $scope.showDocumentAm = true;
    }

    $scope.obrisiAmandman = function (id) {
        console.log(id);

        amandmanFactory.delete(id)
            .then(function (response) {
                for (var i = 0; i < $scope.amandmaniAkta.length; i++)
                    if ($scope.amandmaniAkta[i].idField == id)
                        $scope.amandmaniAkta.splice(i, 1);
                for (var i = 0; i < $scope.amandmani.length; i++)
                    if ($scope.amandmani[i].idField == id)
                        $scope.amandmani.splice(i, 1);
                toastr.success("Uspesno povučen predlog.");
            }, function (error) {
                toastr.error("Greska: " + error.data);
            });

    }
    
}

amandmanModule = angular.module('amandmanModule');
amandmanModule.controller('pregledAkataController', ['$scope', '$sce', '$state', 'aktFactory', 'amandmanFactory', pregledAkataController]);