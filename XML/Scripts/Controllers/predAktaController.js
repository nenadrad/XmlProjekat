﻿predAktaController = function ($scope, aktFactory) {
    
    $scope.submit = function () {
        console.log("submit");
        console.log($scope.dokument);

        var data = {
            dokument: $scope.dokument
        };

        
        aktFactory.insert($scope.dokument)
            .then(function (response) {
                console.log(response.data);
                toastr.success("Uspesno poslat dokument");
                $scope.dokument = "";
            }, function (error) {
                console.log(error.message);
                toastr.error("Greska: " + error.data);
            });
    }

}

var aktModule = angular.module("aktModule");
aktModule.controller('predAktaController', ['$scope', 'aktFactory', predAktaController]);