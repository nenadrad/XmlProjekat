﻿var registerController = function ($scope, $log, authenticationFactory) {
    $scope.register = function () {
        authenticationFactory.register($scope.firstname,$scope.lastName,$scope.username, $scope.password, loginCbck);

        function loginCbck(success) {
            if (success)
                $log.info('success!');
            else
                $log.info('failure!');
        }
    }
}

var registerModule = angular.module('registerModule');
registerModule.controller('registerController',registerController);