﻿var loginController = function ($scope,$log, authenticationFactory,$state) {
    $scope.login = function () {
        authenticationFactory.login($scope.username, $scope.password, loginCbck);

        function loginCbck(success) {
            if (success)
                $log.info('success!');
            else
                $log.info('failure!');
        }
    }

    $scope.registracija = function () {
        $state.go('register');
    }
}

var loginModule = angular.module('loginModule');
loginModule.controller('loginController',loginController);