﻿var pretragaFactory = function ($http) {

    var pretragaUrl = '/api/pretraga';
    var pretragaFactory = {};

    pretragaFactory.search = function (item) {
        return $http({
            method: 'POST',
            url: pretragaUrl,
            data: item,
            //headers: { "Content-Type": 'application/xml', "Accept": "application/xml" }
        })
    };

    return pretragaFactory;
}
var aktModule = angular.module('aktModule');
aktModule.factory('pretragaFactory', pretragaFactory);