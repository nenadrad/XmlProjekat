﻿var aktFactory = function ($http) {

    var aktUrl = '/api/akt';
    var transformUrl = "/api/transform"
    var aktFactory = {};

    aktFactory.getAll = function (uri) {
        return $http.get(aktUrl);
    };

    aktFactory.getHtmlById = function (id) {
        return $http.get(transformUrl + "/" + id);
    }



    //aktFactory.postLogin = function (uri, id) {
    //    return $http.get(urlBase + uri + "/" + id);
    //}

    aktFactory.insert = function (item) {
        return $http({

            method: 'POST',
            url: aktUrl,
            data: item,
            headers: { "Content-Type": 'application/xml', "Accept": "application/xml" }
        })
    };

    aktFactory.update = function (id,item) {
        console.log(item);

        return $http.put(aktUrl + '/' + id, item);
    };

    aktFactory.delete = function (id) {
        return $http.delete(aktUrl + "/" + id);
    };

    //aktFactory.getStavke = function (id) {
    //    return $http.get("/api/stavke/" + id);
    //}


    return aktFactory;
}
var aktModule = angular.module('aktModule');
aktModule.factory('aktFactory', aktFactory);