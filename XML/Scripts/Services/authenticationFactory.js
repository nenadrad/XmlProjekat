﻿var authenticationFactory = function ($http, $state, $location, $stateParams, $localStorage) {
    var service = {};
    var loginUrl = '/api/login';
    service.login = login;
    service.logout = logout;
    service.register = register;
    service.getCurrentUser = getCurrentUser;
    var autentFactory  = {}
    return service;

    /*autentFactory.getLogIn = function (item) {
        return $http({
            method: 'POST',
            url: loginUrl,
            data: item,
            //headers: { "Content-Type": 'application/xml', "Accept": "application/xml" }
        })
    };*/

    function login(u_name, pass, callback) {

        var user = {
            username: u_name,
            password: pass
        }

        $http.post(loginUrl, user)
            .then(function (response) {
                if (response.data == null) {
                    toastr.error("Korisničko ime ili šifra nisu ispravni.");
                    callback(false);
                }
                else {
                    var retUser = response.data;
                    if (retUser.korisnickoImeField == user.username && retUser.lozinkaField == user.password) {
                        $localStorage.currentUser = retUser;
                        //$http.defaults.headers.common.Authorization = response.token;
                        toastr.success("Dobrodosli ");
                        callback(true);
                        $state.go('Home');
                    }
                }
            }, function (error) {
                toastr.error("Greska: " + error.data);
                callback(false);
            });

        /*if (u_name == 'admin' && pass == 'a') {

            var currentUser = {};
            currentUser.username = u_name;
            currentUser.password = pass;

            $localStorage.currentUser = currentUser;

            // jwt token dodajemo u to auth header za sve $http zahteve
            //$http.defaults.headers.common.Authorization = response.token;

            toastr.success("Dobrodosli ");
            callback(true);
            $state.go('Home');

        } else {
            //neuspesan login
            toastr.error("Korisnicko ime ili sifra nisu ispravni");
            callback(false);
        }*/
    }

    function register() {
        console.log("saasas");
        $state.go('register');
    }

    function logout() {
        // uklonimo korisnika iz lokalnog skladišta
        delete $localStorage.currentUser;
    //    $http.defaults.headers.common.Authorization = '';
        $state.go('login');
    }

    function getCurrentUser() {
        return $localStorage.currentUser;
    }

}

var authenticationModule = angular.module('authenticationModule');
authenticationModule.factory('authenticationFactory',authenticationFactory);