﻿var amandmanFactory = function ($http) {

    var amandmanUrl = '/api/amandman';
    var transformUrl = "/api/transform";
    var amandmanFactory = {};

    amandmanFactory.getAll = function () {
        return $http.get(amandmanUrl);
    };

    amandmanFactory.getHtmlById = function (id) {
        return $http.get(transformUrl + "/" + id);
    }

    //amandmanFactory.postLogin = function (uri, id) {
    //    return $http.get(urlBase + uri + "/" + id);
    //}

    amandmanFactory.insert = function (item) {
        return $http({
            method: 'POST',
            url: amandmanUrl,
            data: item
            //headers: { "Content-Type": 'application/xml', "Accept": "application/xml" }
        })
    };

    //amandmanFactory.updateTable = function (uri, id, item) {
    //    return $http.put(urlBase + uri + '/' + id, item)
    //};

    amandmanFactory.delete = function (id) {
        return $http.delete(amandmanUrl + "/" + id);
    };

    //amandmanFactory.getStavke = function (id) {
    //    return $http.get("/api/stavke/" + id);
    //}


    return amandmanFactory;
}
var amandmanModule = angular.module('amandmanModule');
amandmanModule.factory('amandmanFactory', amandmanFactory);