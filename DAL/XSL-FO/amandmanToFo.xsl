<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:amnd="amandmani" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format"
    version="1.0">
    <xsl:template match="/amnd:Amandman">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="myPage" page-width="210mm" page-height="297mm">
                    <fo:region-body margin-bottom="1cm" margin-top="3cm" margin-right="0.340cm" margin-left="0.340cm"/>
                    <fo:region-before extent="20mm"/>
                    <fo:region-after extent="8mm"/>
                    <fo:region-start extent="1mm"/>
                    <fo:region-end extent="1mm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="myPage">
                <fo:flow flow-name="xsl-region-body">
                    <fo:block text-align="center" font-family="Calibri">
                        <fo:block text-align="right">
                            <fo:inline>
                                <xsl:value-of select="amnd:Stanje"/>
                            </fo:inline>
                        </fo:block>
                        <fo:block font-size="22pt" >
                            <fo:inline>
                                Амандман <xsl:value-of select="@id"/>
                            </fo:inline>
                        </fo:block>
                        
                        <fo:block space-before="1cm">
                            У (<xsl:apply-templates select="amnd:NazivAkta"></xsl:apply-templates>) у
                            <xsl:apply-templates select="amnd:Sadrzaj"></xsl:apply-templates>
                        </fo:block>
                        
                        <fo:block>
                            <fo:block text-align="center" space-before="1cm" >Образложење</fo:block>
                            <xsl:apply-templates select="amnd:Obrazlozenje"></xsl:apply-templates>
                        
                        </fo:block>
                        
                        <xsl:apply-templates select="amnd:OvlascenoLice"></xsl:apply-templates>
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    
    <xsl:template match="amnd:NazivAkta">
 
            <xsl:value-of select=" amnd:Tekst"/>
        
    </xsl:template>
    
    <xsl:template match="amnd:Sadrzaj">

            <xsl:apply-templates select="amnd:Clan"></xsl:apply-templates>
        
    </xsl:template>
    
    <xsl:template match="amnd:Clan">

            члан <xsl:value-of select="@id"/>.
            <xsl:apply-templates select="amnd:Stav"></xsl:apply-templates>
            <xsl:apply-templates select="amnd:Tekst"></xsl:apply-templates> 
        
    </xsl:template>
    
    <xsl:template match="amnd:Stav">

            став <xsl:value-of select="@id"/>.
            <xsl:apply-templates select="amnd:Tacka"></xsl:apply-templates>
            <xsl:apply-templates select="amnd:Tekst"></xsl:apply-templates> 
        
    </xsl:template>
    
    
    <xsl:template match="amnd:Tacka">
        тачка <xsl:value-of select="@id"/>.
        <xsl:apply-templates select="amnd:Podtacka"></xsl:apply-templates>
        <xsl:apply-templates select="amnd:Tekst"></xsl:apply-templates> 
        
    </xsl:template>
    
    <xsl:template match="amnd:Podtacka">
        податачка <xsl:value-of select="@id"/>.
        <xsl:apply-templates select="amnd:Alineja"></xsl:apply-templates>
        <xsl:apply-templates select="amnd:Tekst"></xsl:apply-templates> 
        
    </xsl:template>
    
    <xsl:template match="amnd:Alineja">

        алинеја <xsl:value-of select="@id"/>.
        <xsl:apply-templates select="amnd:Tekst"></xsl:apply-templates> 
        
    </xsl:template>
    
    <xsl:template match="amnd:Tekst">

        <xsl:value-of select="text()"/>
        
    </xsl:template>
    
    <xsl:template match="amnd:OvlascenoLice">
        <fo:block text-align="right" space-before="1cm">
           Овлашћено лице: <xsl:value-of select="amnd:Ime"/> <xsl:value-of select="amnd:Prezime"/>
        </fo:block>
    </xsl:template>
    
</xsl:stylesheet>