<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:ak="aktovi" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    
    <xsl:template match="/ak:Akt">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="myPage" page-width="210mm" page-height="297mm">
                    <fo:region-body margin-bottom="1cm" margin-top="3cm" margin-right="0.340cm" margin-left="0.340cm"/>
                    <fo:region-before extent="20mm"/>
                    <fo:region-after extent="8mm"/>
                    <fo:region-start extent="1mm"/>
                    <fo:region-end extent="1mm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            
            <fo:page-sequence master-reference="myPage">
                <fo:flow flow-name="xsl-region-body">
                    <fo:block text-align="center" font-family="Calibri">
                        <fo:block text-align="right">
                            <fo:inline>
                                <xsl:value-of select="ak:Stanje"/>
                            </fo:inline>
                        </fo:block>
                        <fo:block font-size="22pt" >
                            <fo:inline>
                                <xsl:value-of select="ak:Naziv"/>
                            </fo:inline>
                        </fo:block>
                        <fo:block space-after="7mm">
                            Datum: <xsl:value-of select="ak:Datum"/>
                        </fo:block>
                        
                        <xsl:apply-templates select="ak:Preambula"/>
                            
                        <xsl:apply-templates select="ak:Sadrzaj"></xsl:apply-templates>
                            
                        <xsl:apply-templates select="ak:OvlascenoLice"></xsl:apply-templates>
                        
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    
    <xsl:template match="ak:Sadrzaj">
       
            <xsl:apply-templates select="ak:Deo"></xsl:apply-templates>
        
    </xsl:template>
    
    <xsl:template match="ak:Deo">
        <fo:block space-after="5mm" font-size="16pt" space-before="5mm">
            <xsl:value-of select="@naziv"/>
        </fo:block>
            <xsl:apply-templates select="ak:Glava"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="ak:Glava">
        <fo:block space-after="5mm" space-before="5mm" font-size="14pt">
            <fo:inline >            
                <xsl:value-of select="@id"/> <span></span><xsl:value-of select="@naziv"/>
            </fo:inline>
        </fo:block>
            <xsl:apply-templates select="ak:Clan | ak:Odeljak"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="ak:Odeljak">
        <fo:block>
            <xsl:value-of select="@naziv"></xsl:value-of>
        </fo:block>
            <xsl:apply-templates select="ak:Clan |ak:Pododeljak "></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="ak:Pododeljak">
        <fo:block>
            <xsl:value-of select="@naziv"/>
        </fo:block>
            <xsl:apply-templates select="ak:Clan"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="ak:Clan">
        <fo:block text-align="center">
            <xsl:value-of select="@naziv"/> <xsl:value-of select="@id"/>
        </fo:block>
            <xsl:apply-templates select="ak:Stav"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="ak:Stav">
            <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
            <xsl:apply-templates select="ak:Tacka"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="ak:Tacka">
            <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
            <xsl:apply-templates select="ak:Podtacka"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="ak:Podtacka">
            <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
            <xsl:apply-templates select="ak:Alineja"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="ak:Alineja">
            <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="ak:Preambula">
            <xsl:apply-templates select="ak:Referenca"></xsl:apply-templates>
            <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="ak:Referenca">
        <fo:block text-align="left">
            <xsl:value-of select="@refAkt"/>
            <xsl:value-of select="@refClan"/>
            <xsl:value-of select="@refStav"/>
            <xsl:value-of select="@refTacka"/>
            <xsl:value-of select="@refPodtacka"/>
            <xsl:value-of select="@refAlineja"/> 
        </fo:block>
    </xsl:template>
    
    <xsl:template match="ak:Tekst">
        <fo:block space-after="0.5cm" space-before="0.5cm">
            <xsl:value-of select="text()"/>
        </fo:block>
    </xsl:template>
    

    
    <xsl:template match="ak:OvlascenoLice">
        <fo:block  text-align="right">        
            <fo:inline>
            Овлашћено лице: <xsl:value-of select="ak:Ime"/> <xsl:value-of select="ak:Prezime"/>
            </fo:inline>
        </fo:block>
        
    </xsl:template>
    
</xsl:stylesheet>