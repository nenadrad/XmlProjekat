﻿using Marklogic.Xcc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DBConnection
{
    public class MLConnection
    {

        private static Session session;
        private static string username;
        private static string pass;
        private static string host;
        private static int port;
        private static string database;
        private MLConnection() { }

        private static void initialise()
        {
            username = ConfigurationManager.AppSettings["username"];
            pass = ConfigurationManager.AppSettings["password"];
            host = ConfigurationManager.AppSettings["host"];
            int.TryParse(ConfigurationManager.AppSettings["port"], out port);
            database = ConfigurationManager.AppSettings["database"];
        }


        public static Session GetConnection()
        {
            if (session == null || session.Closed)
            {
                try
                {
                    initialise();
                    ContentSource contentSource = ContentSourceFactory.NewContentSource(host, port, username, pass, database);
                    session = contentSource.NewSession();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            return session;
        }

        public static void CloseConnection()
        {
            session.Close();
        }


    }
}
