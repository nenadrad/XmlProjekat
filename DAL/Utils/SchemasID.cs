﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
namespace DAL.Utils
{
    public static class SchemasID
    {
        public static readonly string AKT_NAMESPACE = "aktovi";
        public static readonly string AKT_COLLECTION = "/aktovi";
        public static readonly string AKT_SUFIX = "akt";
        public static readonly string AKT_SCHEMA = HttpContext.Current.Server.MapPath("bin/XMLSchemas/akt.xsd");
        public static readonly string AKT_XSLT = HttpContext.Current.Server.MapPath("bin/XSLTFiles/aktTransform.xsl");
        public static readonly string AKT_XSLFO = HttpContext.Current.Server.MapPath("bin/XSL-FO/aktTofo.xsl");

        public static readonly string AMANDMAN_NAMESPACE = "amandmani";
        public static readonly string AMANDMAN_COLLECTION = "/amandmani";
        public static readonly string AMANDMAN_SUFIX = "amandman";
        public static readonly string AMANDMAN_SCHEMA = HttpContext.Current.Server.MapPath("bin/XMLSchemas/amandman.xsd");
        public static readonly string AMANDMAN_XSLT = HttpContext.Current.Server.MapPath("bin/XSLTFiles/amandmanTrans.xsl");
        public static readonly string AMANDMAN_XSLFO = HttpContext.Current.Server.MapPath("bin/XSL-FO/amandmanToFo.xsl");

        public static readonly string KORISNICI_NAMESPACE = "korisnici";
        public static readonly string KORISNICI_COLLECTION = "/korisnici";
        public static readonly string KORISNICI_SUFIX = "korisnici";
        public static readonly string KORISNICI_SCHEMA = HttpContext.Current.Server.MapPath("bin/XMLSchemas/korisnici.xsd");


    }
}
