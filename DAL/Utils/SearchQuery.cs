﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Utils
{
    public class SearchQuery
    {
        public string TipPretrage { get; set; }
        public string Metapodatak { get; set; }
        public string Tekst { get; set; }
    }
}
