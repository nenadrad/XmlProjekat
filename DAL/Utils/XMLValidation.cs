﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.IO;
namespace DAL.Utils
{
    public class XMLValidation
    {



        public static XmlDocument ValidationBySchema(string inputDoc, string schemaUri,string xmlNamespace)
        {

            XmlSchemaSet schemas = new XmlSchemaSet();
            schemas.Add(xmlNamespace, schemaUri);
            XDocument doc = XDocument.Parse(inputDoc);
            string msg = "";

            doc.Validate(schemas, (o, e) =>
            {

                msg += e.Message + Environment.NewLine;
                Exception ex = new Exception(msg);
                throw ex;           
            });


            XmlDocument document = new XmlDocument();
            document.LoadXml(inputDoc);
            return document;

        }

       


    }
}
