﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DAL.Utils
{
   public class XMLConverter<TEntity> where TEntity : class
    {

        public string Serialize(TEntity dataToSerialize)
        {
            try
            {
                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(typeof(TEntity));
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public TEntity Deserialize(string xmlText)
        {
            try
            {
                var stringReader = new System.IO.StringReader(xmlText);
                var serializer = new XmlSerializer(typeof(TEntity));
                return (TEntity)serializer.Deserialize(stringReader);
            }
            catch(Exception e)
            {
                throw e;
            }
        }



    }
}
