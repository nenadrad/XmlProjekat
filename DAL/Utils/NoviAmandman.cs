﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Utils
{
    public class NoviAmandman
    {
        public string idAkta { get; set; }
        public string amandman { get; set; }

        public Amandman noviAmandman
        {
            get
            {
                XMLConverter<Amandman> amanConvert = new XMLConverter<Amandman>();
                Amandman tempAm = amanConvert.Deserialize(this.amandman);
                tempAm.Sadrzaj.idAkta = this.idAkta;
                return  tempAm;
            }
            set
            {
                this.noviAmandman= value;
            }
        }
    }
}
