<xsl:stylesheet xmlns:ak="aktovi" xmlns:kor="korisnici" xmlns:ulg="uloge" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
  <xsl:output method="html" omit-xml-declaration="yes" encoding="UTF-8"/>
    <xsl:template match="/ak:Akt">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
            <link rel="stylesheet" href="bootstrap.min.css"/>
        </head>
        <html>
            <body>
                <div class="container">
                   <div class="row">
                    <div class="header">
                        <img src="Views/Pictures/novisad.png" alt="Grb Novog Sada"/>
                      
                        <h2><xsl:value-of select="ak:Naziv"/></h2>
                    </div>
                    <div class="well well-sm"><xsl:value-of select="ak:Datum"/></div>
                   
                   <div>
                
                <p><xsl:value-of select="ak:Preambula"/></p> 
                
                <xsl:apply-templates select="ak:Deo"> </xsl:apply-templates>
                
                <xsl:apply-templates select="ak:Predlagac"></xsl:apply-templates>

                <p><xsl:value-of select="ak:Stanje"/></p>
                </div>
                   </div>
               </div>
            </body>
        </html>
        
        
    </xsl:template>
    
    <xsl:template match="ak:Deo">
        <div>
        <h4><xsl:value-of select="@naziv"/></h4>
        <p><xsl:value-of select="@oznaka"/></p>
        <xsl:apply-templates select="ak:Glava"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:Glava">
        <div>
           <h5  align="center"><xsl:value-of select="@oznaka"/><span></span> <xsl:value-of select="@naziv"/></h5>
        <xsl:apply-templates select="ak:Odeljak | ak:Clan"></xsl:apply-templates>
        </div>
    </xsl:template>
   
   <xsl:template match="ak:Odeljak">
       <div>
       <p><xsl:value-of select="@naziv"/></p>
       <p><xsl:value-of select="@oznaka"/></p>
           <xsl:apply-templates select="ak:Clan"></xsl:apply-templates>
           <xsl:apply-templates select="ak:Pododeljak"></xsl:apply-templates>
       </div>
   </xsl:template>
      
    <xsl:template match="ak:Pododeljak">
        <p><xsl:value-of select="@naziv"/></p>
        <p><xsl:value-of select="@oznaka"/></p>
        <xsl:apply-templates select="ak:Clan"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="ak:Clan">
        <div>
           <p align="center"><xsl:value-of select="@naziv"/> <xsl:value-of select="@oznaka"/></p>
            <xsl:apply-templates select="ak:Stav"></xsl:apply-templates>
        </div>
    </xsl:template>
    <xsl:template match="ak:Stav">
        <div>
        <p><xsl:value-of select="@oznaka"/></p>
        <xsl:apply-templates select="ak:Tacka"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:Tacka">
        <div>
        <p><xsl:value-of select="@oznaka"/></p>
        <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
        <xsl:apply-templates select="ak:Podtacka"></xsl:apply-templates>
        </div>
    </xsl:template>
    <xsl:template match="ak:Podtacka">
        <div>
        <p><xsl:value-of select="@oznaka"/></p>
        <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
        <xsl:apply-templates select="ak:Alineja"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:Alineja">
        <div>
            <p><xsl:value-of select="@oznaka"/></p>
            <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
        </div>
    </xsl:template>
    <xsl:template match="ak:Tekst">
        <p><xsl:value-of select="text()"/></p>
    </xsl:template>
  
   
 
  
    
    <xsl:template match="ak:Predlagac">
        <p>Predlagac:</p>
        <p>Narodni poslanik: <xsl:value-of select="kor:Ime"/> <xsl:value-of select="kor:Prezime"/> </p>
        <p>Predtavnik stranka: <xsl:value-of select="ulg:Stranka"/></p>
    </xsl:template>
    
</xsl:stylesheet>