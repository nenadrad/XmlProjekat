<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:ak="aktovi" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output method="html" omit-xml-declaration="yes" encoding="UTF-8"/>
    
    <xsl:template match="/ak:Akt">
        <html>
            <head> 
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <link rel="stylesheet" href="bootstrap.min.css"/></head>
            <body>
                <div class="container">
                    <div class="row">
                        
                        <div class="header">
                            <img src="grb.png" alt="Grb Novog Sada"/>
                            <h5 align="right"> <xsl:value-of select="ak:Stanje"/></h5>
                            <h2><xsl:value-of select="ak:Naziv"/></h2>
                        </div>
                        
                        <div class="well well-sm"><xsl:value-of select="ak:Datum"/></div>
                        
                        <div>
                            <xsl:apply-templates select="ak:Preambula"></xsl:apply-templates>
                            
                            <xsl:apply-templates select="ak:Sadrzaj"></xsl:apply-templates>
                            
                            <xsl:apply-templates select="ak:OvlascenoLice"></xsl:apply-templates>
                            
                            
                        </div>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="ak:Sadrzaj">
        <div>
            <xsl:apply-templates select="ak:Deo"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:Deo">
        <div>
            <h4><xsl:value-of select="@naziv"/></h4>
            <xsl:apply-templates select="ak:Glava"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match=" ak:Glava">
        <div>
            <h5><xsl:value-of select="@id"/> <span></span><xsl:value-of select="@naziv"/></h5>
            <xsl:apply-templates select="ak:Odeljak | ak:Clan"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:Odeljak">
        <div>
        <p><xsl:value-of select="@naziv"></xsl:value-of></p>
            <xsl:apply-templates select="ak:Clan |ak:Pododeljak "></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:Pododeljak">
        <div>
        <p><xsl:value-of select="@naziv"></xsl:value-of></p>
        <xsl:apply-templates select="ak:Clan"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:Clan">
        <div>
        <p align="center"><xsl:value-of select="@naziv"/> <xsl:value-of select="@id"/></p>
        <xsl:apply-templates select="ak:Stav"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:Stav">
        <div>
            <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
            <xsl:apply-templates select="ak:Tacka"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:Tacka">
        <div>
            <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
            <xsl:apply-templates select="ak:Podtacka"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:Podtacka">
        <div>
            <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
            <xsl:apply-templates select="ak:Alineja"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:Alinijea">
        <div>
            <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    
    <xsl:template match="ak:Preambula">
        <div>
            <xsl:apply-templates select="ak:Referenca"></xsl:apply-templates>
            <xsl:apply-templates select="ak:Tekst"></xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:Referenca">
        <div>
            <h5><xsl:value-of select="@refAkt"/></h5>
            <h5><xsl:value-of select="@refClan"/></h5>
            <h5> <xsl:value-of select="@refStav"/></h5>
            <h5><xsl:value-of select="@refTacka"/></h5>
            <h5> <xsl:value-of select="@refPodtacka"/></h5>
            <h5><xsl:value-of select="@refAlineja"/></h5> 
        </div>
    </xsl:template>
    
    <xsl:template match=" ak:Tekst">
        <div>
            <p><xsl:value-of select="text()"/></p>
        </div>
    </xsl:template>
    
    <xsl:template match="ak:OvlascenoLice">
        <div align="right">
            <p> Ovlasceno lice : <xsl:value-of select="ak:Ime"/><xsl:value-of select="ak:Prezime"/></p>
        </div>
    </xsl:template>
    
</xsl:stylesheet>