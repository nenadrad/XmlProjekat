<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:amnd="amandmani" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output method="html" omit-xml-declaration="yes" encoding="UTF-8"/>
    
    <xsl:template match="/amnd:Amandman">
        <html>
            <head> 
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <link rel="stylesheet" href="bootstrap.min.css"/>
            </head>
            <body>
                <div class="container">
                    <div class="row">
                    </div>
                    <div class="header">
                        <h5 align="right"><xsl:value-of select=" amnd:Stanje"/></h5>
                    </div>
                    <div>
                        <h3 align="center">Amandman <xsl:value-of select="@id"/></h3>
                    </div>
                    <div>
                        
                        У (<xsl:apply-templates select="amnd:NazivAkta"></xsl:apply-templates>) у
                          <xsl:apply-templates select="amnd:Sadrzaj"></xsl:apply-templates>
                    <div>
                        <h5 align="center">Obrazlozenje</h5>
                       <p><xsl:value-of select="amnd:Obrazlozenje"></xsl:value-of>
                       </p>
                    </div>
                       <xsl:apply-templates select="amnd:OvlascenoLice"></xsl:apply-templates>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
        
        <xsl:template match="amnd:Sadrzaj"> 
                <xsl:apply-templates select="amnd:Clan"></xsl:apply-templates>
        </xsl:template>
    
    <xsl:template match="amnd:NazivAkta">
           <xsl:apply-templates select="amnd:Tekst"></xsl:apply-templates> 
    </xsl:template>
    
    <xsl:template match="amnd:Clan">
        члан <xsl:value-of select="@id"/>.
        <xsl:apply-templates select="amnd:Stav"></xsl:apply-templates>
        <xsl:apply-templates select="amnd:Tekst"></xsl:apply-templates> 
    </xsl:template>
 
    <xsl:template match=" amnd:Stav">
        став <xsl:value-of select="@id"/>.
        <xsl:apply-templates select="amnd:Tacka"></xsl:apply-templates>
        <xsl:apply-templates select="amnd:Tekst"></xsl:apply-templates> 
        
    </xsl:template>
     
    <xsl:template match="amnd:Tacka">
        тачка <xsl:value-of select="@id"/>.
        <xsl:apply-templates select="amnd:Podtacka"></xsl:apply-templates>
        <xsl:apply-templates select="amnd:Tekst"></xsl:apply-templates> 
    </xsl:template>

    
    <xsl:template match="amnd:Podtacka">
        подтачка <xsl:value-of select="@id"/>.
        <xsl:apply-templates select="amnd:Alineja"></xsl:apply-templates>
        <xsl:apply-templates select="amnd:Tekst"></xsl:apply-templates> 
    </xsl:template>
    
    <xsl:template match="amnd:Alineja">
        алинеја <xsl:value-of select="@id"/>.
        <xsl:apply-templates select="amnd:Tekst"></xsl:apply-templates> 
    </xsl:template>
    
    <xsl:template match="amnd:Tekst">
                <xsl:value-of select="text()"/>
    </xsl:template>

    
    <xsl:template match="amnd:OvlascenoLice">
        <div>
        <p align="right">
            Народни посланик: <xsl:value-of select="amnd:Ime"/> <xsl:value-of select=" amnd:Prezime"/>
        </p>
        </div>
    </xsl:template>
    
</xsl:stylesheet>