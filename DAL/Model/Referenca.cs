/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="aktovi")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="aktovi", IsNullable=false)]
public partial class Referenca {
    
    private string refAktField;
    
    private string refClanField;
    
    private string refStavField;
    
    private string refTackaField;
    
    private string refPodtackaField;
    
    private string refAlinejaField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string refAkt {
        get {
            return this.refAktField;
        }
        set {
            this.refAktField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string refClan {
        get {
            return this.refClanField;
        }
        set {
            this.refClanField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string refStav {
        get {
            return this.refStavField;
        }
        set {
            this.refStavField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string refTacka {
        get {
            return this.refTackaField;
        }
        set {
            this.refTackaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string refPodtacka {
        get {
            return this.refPodtackaField;
        }
        set {
            this.refPodtackaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string refAlineja {
        get {
            return this.refAlinejaField;
        }
        set {
            this.refAlinejaField = value;
        }
    }
}