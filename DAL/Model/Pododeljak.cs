/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="aktovi")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="aktovi", IsNullable=false)]
public partial class Pododeljak {
    
    private Clan[] clanField;
    
    private string idField;
    
    private string nazivField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("Clan")]
    public Clan[] Clan {
        get {
            return this.clanField;
        }
        set {
            this.clanField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id {
        get {
            return this.idField;
        }
        set {
            this.idField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string naziv {
        get {
            return this.nazivField;
        }
        set {
            this.nazivField = value;
        }
    }
}