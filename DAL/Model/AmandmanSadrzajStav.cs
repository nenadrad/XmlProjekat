/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="amandmani")]
public partial class AmandmanSadrzajStav {
    
    private string tekstField;
    
    private string idClanaField;
    
    private string idStavaField;
    
    /// <remarks/>
    public string Tekst {
        get {
            return this.tekstField;
        }
        set {
            this.tekstField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string idClana {
        get {
            return this.idClanaField;
        }
        set {
            this.idClanaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string idStava {
        get {
            return this.idStavaField;
        }
        set {
            this.idStavaField = value;
        }
    }
}