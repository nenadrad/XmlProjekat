/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="aktovi")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="aktovi", IsNullable=false)]
public partial class Podtacka {
    
    private string tekstField;
    
    private Alineja[] alinejaField;
    
    private Referenca[] referencaField;
    
    private string idField;
    
    /// <remarks/>
    public string Tekst {
        get {
            return this.tekstField;
        }
        set {
            this.tekstField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("Alineja")]
    public Alineja[] Alineja {
        get {
            return this.alinejaField;
        }
        set {
            this.alinejaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("Referenca")]
    public Referenca[] Referenca {
        get {
            return this.referencaField;
        }
        set {
            this.referencaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id {
        get {
            return this.idField;
        }
        set {
            this.idField = value;
        }
    }
}