/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="aktovi")]
public enum AktStanje {
    
    /// <remarks/>
    PREDLOZEN,
    
    /// <remarks/>
    U_PROCEDURI,
    
    /// <remarks/>
    ODBIJEN,
    
    /// <remarks/>
    GLASANJE,
    
    /// <remarks/>
    PRIHVACEN,
    
    /// <remarks/>
    PRIHVACEN_DEL,
}