/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="amandmani")]
public partial class AmandmanSadrzaj {
    
    private TOperacija operacijaField;
    
    private AmandmanSadrzajClan clanField;
    
    private string idAktaField;
    
    /// <remarks/>
    public TOperacija Operacija {
        get {
            return this.operacijaField;
        }
        set {
            this.operacijaField = value;
        }
    }
    
    /// <remarks/>
    public AmandmanSadrzajClan Clan {
        get {
            return this.clanField;
        }
        set {
            this.clanField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string idAkta {
        get {
            return this.idAktaField;
        }
        set {
            this.idAktaField = value;
        }
    }
}