/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="uloge")]
public partial class TPredsednik : TOdbornik {
    
    private string mandatField;
    
    /// <remarks/>
    public string Mandat {
        get {
            return this.mandatField;
        }
        set {
            this.mandatField = value;
        }
    }
}