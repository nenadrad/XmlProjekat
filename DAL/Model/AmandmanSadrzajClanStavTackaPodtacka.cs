/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="amandmani")]
public partial class AmandmanSadrzajClanStavTackaPodtacka {
    
    private string tekstField;
    
    private AmandmanSadrzajClanStavTackaPodtackaAlineja alinejaField;
    
    private string idField;
    
    /// <remarks/>
    public string Tekst {
        get {
            return this.tekstField;
        }
        set {
            this.tekstField = value;
        }
    }
    
    /// <remarks/>
    public AmandmanSadrzajClanStavTackaPodtackaAlineja Alineja {
        get {
            return this.alinejaField;
        }
        set {
            this.alinejaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id {
        get {
            return this.idField;
        }
        set {
            this.idField = value;
        }
    }
}