/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="korisnici")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="korisnici", IsNullable=false)]
public partial class Korisnici {
    
    private TKorisnik[] korisnikField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("Korisnik")]
    public TKorisnik[] Korisnik {
        get {
            return this.korisnikField;
        }
        set {
            this.korisnikField = value;
        }
    }
}