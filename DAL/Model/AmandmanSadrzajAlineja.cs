/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="amandmani")]
public partial class AmandmanSadrzajAlineja {
    
    private string tekstField;
    
    private string idClanaField;
    
    private string idStavaField;
    
    private string idTackeField;
    
    private string idPodtackeField;
    
    private string idAlinejeField;
    
    /// <remarks/>
    public string Tekst {
        get {
            return this.tekstField;
        }
        set {
            this.tekstField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string idClana {
        get {
            return this.idClanaField;
        }
        set {
            this.idClanaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string idStava {
        get {
            return this.idStavaField;
        }
        set {
            this.idStavaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string idTacke {
        get {
            return this.idTackeField;
        }
        set {
            this.idTackeField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string idPodtacke {
        get {
            return this.idPodtackeField;
        }
        set {
            this.idPodtackeField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string idAlineje {
        get {
            return this.idAlinejeField;
        }
        set {
            this.idAlinejeField = value;
        }
    }
}