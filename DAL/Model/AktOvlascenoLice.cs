/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="aktovi")]
public partial class AktOvlascenoLice {
    
    private string imeField;
    
    private string prezimeField;
    
    private string usernameField;
    
    /// <remarks/>
    public string Ime {
        get {
            return this.imeField;
        }
        set {
            this.imeField = value;
        }
    }
    
    /// <remarks/>
    public string Prezime {
        get {
            return this.prezimeField;
        }
        set {
            this.prezimeField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string username {
        get {
            return this.usernameField;
        }
        set {
            this.usernameField = value;
        }
    }
}