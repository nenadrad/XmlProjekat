/// <remarks/>
[System.Xml.Serialization.XmlIncludeAttribute(typeof(TPredsednik))]
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="uloge")]
public partial class TOdbornik : TGradjanin {
    
    private string strankaField;
    
    /// <remarks/>
    public string Stranka {
        get {
            return this.strankaField;
        }
        set {
            this.strankaField = value;
        }
    }
}