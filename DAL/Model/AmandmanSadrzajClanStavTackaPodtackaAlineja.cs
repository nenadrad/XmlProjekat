/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="amandmani")]
public partial class AmandmanSadrzajClanStavTackaPodtackaAlineja {
    
    private string tekstField;
    
    private string idField;
    
    /// <remarks/>
    public string Tekst {
        get {
            return this.tekstField;
        }
        set {
            this.tekstField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id {
        get {
            return this.idField;
        }
        set {
            this.idField = value;
        }
    }
}