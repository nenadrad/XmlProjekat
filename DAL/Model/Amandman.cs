/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="amandmani")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="amandmani", IsNullable=false)]
public partial class Amandman {
    
    private AmandmanNazivAkta nazivAktaField;
    
    private AmandmanSadrzaj sadrzajField;
    
    private string obrazlozenjeField;
    
    private AmandmanOvlascenoLice ovlascenoLiceField;
    
    private AmandmanStanje stanjeField;
    
    private string idField;
    
    /// <remarks/>
    public AmandmanNazivAkta NazivAkta {
        get {
            return this.nazivAktaField;
        }
        set {
            this.nazivAktaField = value;
        }
    }
    
    /// <remarks/>
    public AmandmanSadrzaj Sadrzaj {
        get {
            return this.sadrzajField;
        }
        set {
            this.sadrzajField = value;
        }
    }
    
    /// <remarks/>
    public string Obrazlozenje {
        get {
            return this.obrazlozenjeField;
        }
        set {
            this.obrazlozenjeField = value;
        }
    }
    
    /// <remarks/>
    public AmandmanOvlascenoLice OvlascenoLice {
        get {
            return this.ovlascenoLiceField;
        }
        set {
            this.ovlascenoLiceField = value;
        }
    }
    
    /// <remarks/>
    public AmandmanStanje Stanje {
        get {
            return this.stanjeField;
        }
        set {
            this.stanjeField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id {
        get {
            return this.idField;
        }
        set {
            this.idField = value;
        }
    }
}