/// <remarks/>
[System.Xml.Serialization.XmlIncludeAttribute(typeof(TGradjanin))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(TOdbornik))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(TPredsednik))]
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="korisnici")]
[System.Xml.Serialization.XmlRootAttribute("Korisnik", Namespace="korisnici", IsNullable=false)]
public partial class TKorisnik {
    
    private string imeField;
    
    private string prezimeField;
    
    private string emailField;
    
    private string korisnickoImeField;
    
    private string lozinkaField;
    
    private TNazivUloge ulogaField;
    
    private string idField;
    
    /// <remarks/>
    public string Ime {
        get {
            return this.imeField;
        }
        set {
            this.imeField = value;
        }
    }
    
    /// <remarks/>
    public string Prezime {
        get {
            return this.prezimeField;
        }
        set {
            this.prezimeField = value;
        }
    }
    
    /// <remarks/>
    public string Email {
        get {
            return this.emailField;
        }
        set {
            this.emailField = value;
        }
    }
    
    /// <remarks/>
    public string KorisnickoIme {
        get {
            return this.korisnickoImeField;
        }
        set {
            this.korisnickoImeField = value;
        }
    }
    
    /// <remarks/>
    public string Lozinka {
        get {
            return this.lozinkaField;
        }
        set {
            this.lozinkaField = value;
        }
    }
    
    /// <remarks/>
    public TNazivUloge Uloga {
        get {
            return this.ulogaField;
        }
        set {
            this.ulogaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id {
        get {
            return this.idField;
        }
        set {
            this.idField = value;
        }
    }
}