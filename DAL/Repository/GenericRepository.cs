﻿using Marklogic.Xcc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Utils;
using System.Xml;
using System.Xml.Xsl;
using System.IO;

namespace DAL.Repository
{   

    
    public class GenericRepository<TEntity> where TEntity : class
    {
        private Session session;
        private string uriSufix;
        private string collection;
        private string schemaUri;
        string xmlNamespace;
         XMLConverter<TEntity> xmlConverter = new XMLConverter<TEntity>();
        private ContentCreateOptions options = ContentCreateOptions.NewXmlInstance();
        private string header = "xquery version \"1.0-ml\";" + System.Environment.NewLine;
        

        public XMLConverter<TEntity> XMLConveretr { get { return xmlConverter; } }

        public GenericRepository(Session session, string uriSufix,string collection, string schemaUri,string xmlNamespace)
        {
            this.session = session;
            this.uriSufix = uriSufix;
            this.collection = collection;
            this.schemaUri = schemaUri;
            this.xmlNamespace = xmlNamespace;
            options.Collections =new string[]{ collection};

        }

        //fn:generate-id($x)
      

        public IEnumerable<TEntity> Get() 
        {
            List<TEntity> retList = new List<TEntity>();
            Request request = session.NewAdhocQuery(header + "fn:collection('" + this.collection + "')");
            ResultSequence rs = session.SubmitRequest(request);

            while (rs.HasNext())
            {
                try
                {
                    ResultItem rsItem = rs.Next();
                    XmlDocument doc = XMLValidation.ValidationBySchema(rsItem.AsString(), schemaUri,xmlNamespace);
                    TEntity entity = xmlConverter.Deserialize(rsItem.AsString());
                    retList.Add(entity);
                }catch(Exception e)
                {
                    throw e;
                }
            }
            
            return retList;
        }

        // Request request = session.NewAdhocQuery("fn:doc('probasdada.xml')");
        
        public TEntity GetById(string id)
        {
            TEntity entity = null;
            string docUri = id;
            Request request = session.NewAdhocQuery(header + "fn:doc('" + docUri + "')");
            ResultSequence rs = session.SubmitRequest(request);

            while (rs.HasNext())
            {
                ResultItem rsItem = rs.Next();
                try
                {
                    XmlDocument doc = XMLValidation.ValidationBySchema(rsItem.AsString(), schemaUri,xmlNamespace);
                    entity = xmlConverter.Deserialize(rsItem.AsString());

                }catch(Exception e)
                {
                    throw e;
                }
            }

            return entity;

        }

        public XmlDocument GetByIdxml(string id)
        {
            XmlDocument entity = null;
            string docUri = id;
            Request request = session.NewAdhocQuery(header + "fn:doc('" + docUri + "')");
            ResultSequence rs = session.SubmitRequest(request);

            if (rs.HasNext())
            {
                ResultItem rsItem = rs.Next();
                try
                {
                    entity = XMLValidation.ValidationBySchema(rsItem.AsString(), schemaUri, xmlNamespace);            
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return entity;

        }




        public void DoQuery(string query)
        {

            try
            {
                Request request = session.NewAdhocQuery(query);
                ResultSequence rs = session.SubmitRequest(request);
            }
            catch (Exception e)
            {
                throw e;
            }


        }


        public IEnumerable<TEntity> DoQueryWithRetyrn(string query)
        {

            try
            {
                List<TEntity> retList = new List<TEntity>();
                Request request = session.NewAdhocQuery(query);
                ResultSequence rs = session.SubmitRequest(request);

                while (rs.HasNext())
                {
                     ResultItem rsItem = rs.Next();
                     XmlDocument doc = XMLValidation.ValidationBySchema(rsItem.AsString(), schemaUri, xmlNamespace);
                     TEntity entity = xmlConverter.Deserialize(rsItem.AsString());
                     retList.Add(entity);
                
                }
                return retList;
            }
            catch (Exception e)
            {
                throw e;
            }        
        }


        public string DoQueryRetString(string query)
        {

            try
            {
                string retList = null;
                Request request = session.NewAdhocQuery(query);
                ResultSequence rs = session.SubmitRequest(request);

                while (rs.HasNext())
                {
                    ResultItem rsItem = rs.Next();
                    retList = rsItem.AsString();

                }
                return retList;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public String GetByIdString(string id)
        {
            string entity = null;
            string docUri = id;
            Request request = session.NewAdhocQuery(header + "fn:doc('" + docUri + "')");
            ResultSequence rs = session.SubmitRequest(request);
            options.Collections =new string[] { this.collection };
            while (rs.HasNext())
            {
                ResultItem rsItem = rs.Next();
                try
                {
                    entity = rsItem.AsString();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return entity;

        }





        public void Insert(TEntity entity)
        {
            string id = this.GenerateId();
            string uri = id + this.uriSufix;
            string inputDoc = xmlConverter.Serialize(entity);
            XmlDocument document = new XmlDocument();
            try {
                document = XMLValidation.ValidationBySchema(inputDoc, this.schemaUri,xmlNamespace);
            }catch(Exception e)
            {
                throw e;
            }
            if (document.DocumentElement.Attributes["id"] != null)
            {
                document.DocumentElement.Attributes["id"].Value = uri;
            }
            else
            {
                // document.DocumentElement.Attributes["id"]
                XmlAttribute newAttribute = document.CreateAttribute("id");
                newAttribute.Value = uri;
                document.DocumentElement.Attributes.Append(newAttribute);


            }         
            Content content = ContentFactory.NewContent(uri, document, options);
            try
            {
                session.InsertContent(content);
            }
            catch (Exception e)
            {
                throw;
            }
        }



        public void InsertXml(XmlDocument entity)
        {
 
            string uri = entity.DocumentElement.Attributes["id"].Value;
            string inputDoc = entity.InnerXml;
            XmlDocument document = new XmlDocument();
            try
            {
                document = XMLValidation.ValidationBySchema(inputDoc, this.schemaUri, xmlNamespace);
            }
            catch (Exception e)
            {
                throw e;
            }
         
            Content content = ContentFactory.NewContent(uri, document, options);
            try
            {
                session.InsertContent(content);
            }
            catch (Exception e)
            {
                throw;
            }
        }






        public void Delete(string id)
        {
            string uri = id;
            //xdmp:document-delete("example.xml")
            try {
                Request request = session.NewAdhocQuery(header + "xdmp:document-delete('"+uri+"')");              
                ResultSequence rs = session.SubmitRequest(request);
            }catch(Exception e)
            {
                throw;
            }

        }

        private string GenerateId()
        {
            try
            {
                Request request = session.NewAdhocQuery(header + "xdmp:request()");
                ResultSequence rs = session.SubmitRequest(request);
                ResultItem rsItem = rs.Next();

                return rsItem.AsString();
            }catch(Exception e)
            {
                throw;
            }
           
        }

        public string GetXslString(string doId)
        {
            string ret;
            try
            {
                string inputXml = GetByIdString(doId);
                ret = TransformXMLToHTML(inputXml);
                return ret;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private  string TransformXMLToHTML(string inputXml)
        {

            XslCompiledTransform transform = new XslCompiledTransform();
            try
            {
                using (XmlReader reader = XmlReader.Create(SchemasID.AKT_XSLT))
                {
            
                    transform.Load(reader);
                }
                StringWriter results = new StringWriter();
                using (XmlReader reader = XmlReader.Create(new StringReader(inputXml)))
                {
                    transform.Transform(reader, null, results);
                }
                return results.ToString();
            }
            catch(Exception e)
            {
                throw e;
            }
         
        }



    }
}
