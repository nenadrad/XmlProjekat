﻿using Marklogic.Xcc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.DBConnection;
using System.Diagnostics;
using DAL.Utils;
namespace DAL.Repository
{
   public class UnitOfWork : IDisposable
    {



        private GenericRepository<Akt> aktRepository;
        private GenericRepository<Amandman> amandmanRepository;
        private GenericRepository<Korisnici> korisniciRepository;
        Session session;
        public UnitOfWork()
        {
            session = MLConnection.GetConnection();
        }


        public GenericRepository<Akt> AktRepository
        {
            get
            {
                if (this.aktRepository == null)
                    this.aktRepository = new GenericRepository<Akt>(session, SchemasID.AKT_SUFIX, SchemasID.AKT_COLLECTION,
                                                                     SchemasID.AKT_SCHEMA,SchemasID.AKT_NAMESPACE) ;
                return aktRepository;
            }
        }



        public GenericRepository<Amandman> AmandmanRepository
        {
            get
            {
                if (this.amandmanRepository == null)
                    this.amandmanRepository = new GenericRepository<Amandman>(session, SchemasID.AMANDMAN_SUFIX, SchemasID.AMANDMAN_COLLECTION,
                                                                     SchemasID.AMANDMAN_SCHEMA,SchemasID.AMANDMAN_NAMESPACE);
                return amandmanRepository;
            }
        }

        public GenericRepository<Korisnici> KorisniciRepository
        {
            get
            {
                if (this.korisniciRepository == null)
                    this.korisniciRepository = new GenericRepository<Korisnici>(session, SchemasID.KORISNICI_SUFIX, SchemasID.KORISNICI_COLLECTION,
                                                                     SchemasID.KORISNICI_SCHEMA, SchemasID.KORISNICI_NAMESPACE);
                return korisniciRepository;
            }
        }




        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    session.Dispose();
                }
            }
            this.disposed = true;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
